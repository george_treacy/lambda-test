package co.redeye.core.filer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by george on 25/09/15.
 */
public class MySqlConnector {

    private static final Logger logger = LoggerFactory.getLogger(MySqlConnector.class);

    // private static final String remoteConnectionString = "jdbc:mysql://george-rds.cjosmri8vcnm.ap-southeast-2.rds.amazonaws.com/redeye?user=george&password=today123";
    private static final String remoteConnectionString = "jdbc:mysql://geo-001.cjkh6rtk2yht.us-west-2.rds.amazonaws.com/masso?user=george&password=Yellowsun22";

    private static Connection conn;

    public static Connection getConnection() {
        if (conn == null) {
            createConnection(remoteConnectionString);
        }
        return conn;
    }

    private static void createConnection(final String connectionString) {
        try {
            conn = DriverManager.getConnection(connectionString);
        } catch (SQLException ex) {
            logger.debug("SQLException: " + ex.getMessage());
            logger.debug("SQLState: " + ex.getSQLState());
            logger.debug("VendorError: " + ex.getErrorCode());
            throw new RuntimeException("MySQL connection failed.");
        }
    }

}
