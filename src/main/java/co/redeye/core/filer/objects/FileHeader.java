package co.redeye.core.filer.objects;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by george on 25/09/15.
 */
public class FileHeader {

    private String bucket;
    private String key;
    private String eTag;
    private Long size;

    FileHeader() {
    }

    public FileHeader(final String bucket, final String key, final String eTag, final Long size) {
        this.bucket = bucket;
        this.key = key;
        this.eTag = eTag;
        this.size = size;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String geteTag() {
        return eTag;
    }

    public void seteTag(String eTag) {
        this.eTag = eTag;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
