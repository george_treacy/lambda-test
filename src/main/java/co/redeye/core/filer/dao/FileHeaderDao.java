package co.redeye.core.filer.dao;

import co.redeye.core.filer.objects.FileHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by george on 25/09/15.
 */
public class FileHeaderDao {

    private static final Logger logger = LoggerFactory.getLogger(FileHeaderDao.class);

    private Connection connection;

    public FileHeaderDao(final Connection connection) {
        this.connection = connection;
    }

    public void create(final FileHeader fileHeader) throws SQLException {
        String sql = "insert into file_header (etag, bucket, keey, size_bytes) values (?, ?, ?, ?)";

        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, fileHeader.geteTag());
        statement.setString(2, fileHeader.getBucket());
        statement.setString(3, fileHeader.getKey());
        statement.setLong(4, fileHeader.getSize());

        if (statement.executeUpdate() == 0) {
            logger.error("Create file header failed.");
        }
    }

    public FileHeader getByEtag(final String etag) {
        FileHeader fileHeader = null;
        try {
            final String sql = "select * from file_header where etag = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, etag);
            try (ResultSet rs = statement.executeQuery();) {
                while (rs.next()) {
                    final String bucket = rs.getString("bucket");
                    final String key = rs.getString("keey");
                    fileHeader = new FileHeader(etag, bucket, key, 0L);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fileHeader;
    }


}
