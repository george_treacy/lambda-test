package co.redeye.core.filer;

import co.redeye.core.filer.dao.FileHeaderDao;
import co.redeye.core.filer.objects.FileHeader;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;

import java.io.IOException;
import java.net.URLDecoder;

public class S3EventProcessor implements RequestHandler<S3Event, String> {

    private FileHeaderDao fileHeaderDao;

    public S3EventProcessor() {
        fileHeaderDao = new FileHeaderDao(MySqlConnector.getConnection());
    }

    public String handleRequest(S3Event s3event, Context context) {
        LambdaLogger logger = context.getLogger();

        try {
            S3EventNotificationRecord record = s3event.getRecords().get(0);

            final String bucket = record.getS3().getBucket().getName();
            // Object key may have spaces or unicode non-ASCII characters.
            String key = record.getS3().getObject().getKey().replace('+', ' ');
            key = URLDecoder.decode(key, "UTF-8");

            final String etag = record.getS3().getObject().geteTag();

            FileHeader fileHeader = null;
            try {
                fileHeader = new FileHeader(bucket, key, etag, 0L);
                logger.log(fileHeader.toString());
                fileHeaderDao.create(fileHeader);
            } catch (Exception e) {
                // This is a bit lame. But if anything goes wrong there is nothing we can do and we just want to keep going.
                // report it and keep going.
                logger.log(e.getMessage());
                e.printStackTrace();
            }

            return "Ok"; // for testing only
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}